const express = require('express');
const router = express.Router();
const request = require('request'); // "Request" library
const fs = require('fs'); //Filesystem library

/* Spotify Client Credentials */
const client_id = '4ee5a840dc564860858f9af699005b5a'; // Your client id
const client_secret = '2d18ef9b671a476f973451e298216dd6'; // Your secret
const redirect_uri = 'http://localhost:3000/callback/'; // Your redirect uri

/* Get spotify access token */
const authOptions = {
  url: 'https://accounts.spotify.com/api/token',
  headers: {
    'Authorization': 'Basic ' + (new Buffer(client_id + ':' + client_secret).toString('base64'))
  },
  form: {
    grant_type: 'client_credentials'
  },
  json: true
};

/* Generic basic strip function to remove special chars & toLower */
const stripped = str => {
  let temp = str.replace(/[^\w\s]/gi, '');
  return temp.replace(/[.,\/#()\[\]?!$%\^&\*;:{}=\-_`~()]/g, "").toLowerCase();
}

/* Return ID from spotify URL */
const linkToId = url => {
  return url.indexOf('spotify.com') + url.indexOf('/playlist/') < 0 ? false : url.substr(url.indexOf("/playlist/") + 10, 22);
}

const throwError = (error, res) => {
  res.render('error', {
    message: error.message,
    error: error
  });
  console.log(error);
  return;
}

/* POST Conversion page. */
router.post('/', function (req, res, next) {

  let playlistID = linkToId(req.body.playlistURL);
  console.log(`Playlist ID: ${playlistID}`);

  if (!playlistID) {
    throwError({
      message: `${req.body.playlistURL} is not a valid playlist URL`,
      status: 400
    }, res)
    return;
  }

  let playlist = {};

  //get playlist info POST
  request.post(authOptions, function (error, response, req) {

    var testes = res;

    //throw error if result is not expected
    if (error || response.statusCode !== 200) {
      throwError(error, response);
      return;
    }

    // use the access token to access the Spotify Web API
    var token = req.access_token;

    //set up options used to access playlist info
    var options = {
      url: `https://api.spotify.com/v1/playlists/${playlistID}/?fields=images%2Cdescription%2Cfollowers(total)%2Cid%2Cname%2Cowner(display_name)%C2tracks(total%2Citems(is_local%2Ctrack(name%C2id%2Calbum(name%2Cid)%2Cartists)))`,
      headers: {
        'Authorization': 'Bearer ' + token //Bearer & token from auth POST
      },
      json: true //return in JSON format
    };

    request.get(options, function (error, response, req) {

      //console.log(`GET DEBUG:
      // error: ${JSON.stringify(error)}
      //response: ${JSON.stringify(response)}
      //body: ${JSON.stringify(req)}`);

      //throw error if result is not expected
      if (error || response.statusCode !== 200) {
        throwError(error, response);
        return;
      }

      playlist.name = req.name;
      playlist.description = req.description;
      playlist.followers = req.followers.total;
      playlist.numTracks = req.tracks.total;
      playlist.creator = req.owner.display_name;
      playlist.tracks = req.tracks.items;
      playlist.image = req.images[0].url;

      console.log(JSON.stringify(playlist.name));

      testes.render('convert', {
        title: `SpotDeez - ${playlist.name}`,
        playlist: playlist,
        playlist_id: playlistID
      });
    });
  });
});

router.post('/deezer', function (req, res, next) {

  //list of deezer album IDs of matching items
  let deezerList = {};

  //get playlist information and then iterate through playlist to get deezer matches
  req.playlist.tracks.forEach(song => {

    //create stripped data object for onsensitive compare on GET results from deezer
    let strippedSong = {
      artist: stripped(song.track.artists[0].name),
      album: stripped(song.track.album.name),
      track: stripped(song.track.name)
    }

    //declare the search options, json: true is required as it returns an unknown format otherwise
    let searchOptions = {
      url: `https://api.deezer.com/search/album?q=artist:"${strippedSong.album}" album:"${strippedSong.artist}"`,
      json: true
    }

    request.get(searchOptions, function (res, req, next) {

      //if returned data is invalid then escape.
      if (!req.data[0]) {
        throwError(next, res);
        return false;
      }

      //iterate through results, check spotify info against deezer results. if match found, push to deezer ID object "deezerList"
      req.data.forEach(result => {
        console.log(`Deezer: ${result.title} - Original: ${strippedSong.album}`);
        if (stripped(result.title) != strippedSong.album) {
          console.log();
        }
      })

    })

    deezerList.push();
  });

  res.render('convert', {
    title: `SpotDeez`,
    playlist: {},
    playlist_id: null
  })
});

module.exports = router;